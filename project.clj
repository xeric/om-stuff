(defproject xeric/om-stuff "0.1.0-SNAPSHOT"
  :description "om helper module"
  :url "http://bitbucket.org/xeric/om-stuff"
  :jvm-target "1.6"
  :jvm-opts ["-Dfile.encoding=UTF-8"]
  :dependencies [[org.clojure/clojure "1.7.0-beta3"]
                 [org.clojure/clojurescript "0.0-3308"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [org.omcljs/om "0.9.0" :exclusions [cljsjs/react]]
                 [cljsjs/react-with-addons "0.13.3-0"]
                 [kioo "0.4.1" :exclusions [org.omcljs/om]]
                 [com.cemerick/clojurescript.test "0.3.3" :scope "test"]]

  :plugins [[lein-cljsbuild "1.0.5" :exclusions [org.clojure/clojure]]]
  :clean-targets ^{:protect false} ["target/js/compiled" "target"]
  :cljsbuild
  {:builds [{:id "dev"
             :source-paths ["src" "test"]
             :compiler {:main om-stuff.demo
                        :warnings true 
                        :optimizations :none
                        :asset-path "js/compiled/out"
                        :output-to "demo/js/compiled/om_stuff.js"
                        :output-dir "demo/js/compiled/out"
                        :source-map true }}
            {:id "test"
             :source-paths ["src" "test"]
             :notify-command
             ["phantomjs" :cljs.test/runner
              "--web-security=false"
              "this.literal_js_was_evaluated=true"
              "--local-to-remote-url-access=true"
              "test-resources/js/polyfill.js"
              "test-resources/js/sortable.min.js"
              "test-resources/js/react-with-addons.js"
              "target/cljs/testable.js"]
             :compiler {:output-to "target/cljs/testable.js"
                        :main om-stuff.demo            
                        :optimizations :simple
                        :pretty-print false}}]})
