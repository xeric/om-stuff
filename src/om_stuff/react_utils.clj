(ns om-stuff.react-utils
  (:require [kioo.om :refer [defsnippet deftemplate component]]
            [clojure.string :as string]))

(defn template-path
  "Resolves template-path. The template-path is assumed to be on
  the classpath with the same directory structure as the namespace"
  []
  (if-let [path (-> (meta *ns*) :template-path)]
    path
    (-> (->> (string/split (str *ns*) #"\.") (string/join "/") (str "templates/"))
        (str ".html")
        (string/replace #"-" "_"))))

(defmacro kioo-component
  [sel args]
  `(component ~(template-path) ~sel
              ~(assoc args [(list 'attr? :data-message)]
                      (list 'om-stuff.react-utils/translate-messages))))
