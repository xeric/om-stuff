(ns om-stuff.react-utils
  (:require [kioo.om :refer [do-> content add-class listen set-attr]]
            [om.core :as om :include-macros true]))

(defn value [e]
  (-> e .-target .-value))

(defn checked? [e]
  (-> e .-target .-checked))

(defn hide
  ([hide?] (hide hide? "block"))
  ([hide? display]
   (if hide?
     #js {:display "none"}
     #js {:display display})))  

(defn form-el
  [owner key]
  (do-> (set-attr :value (om/get-state owner key))
        (listen :on-change
                #(om/set-state! owner key (value %)))))

(defn form-el-checked
  [owner key]
  (do-> (set-attr :checked (om/get-state owner key))
        (listen :on-change
                #(om/set-state! owner key (checked? %)))))

(defn form-el-unchecked
  [owner key]
  (do-> (set-attr :checked (not (om/get-state owner key)))
        (listen :on-change
                #(om/set-state! owner key (not (checked? %))))))

(defn kioo-disabled
  [disabled?]
  (if disabled?
    (set-attr :disabled "disabled")
    identity))

(defn kioo-hide
  ([hide?] (kioo-hide hide? "block"))
  ([hide? display]
   (set-attr :style (hide hide? display))))

