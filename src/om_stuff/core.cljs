(ns om-stuff.core
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan <!]]
            [om-stuff.react-utils :as react-utils]))

(defmulti form-element (fn [value _ {:keys [field]}] (:type field)))

(defmulti form-group (fn [_ _ {:keys [group]}] group))

(defn- error-icon [error]
  (if error
    (dom/span
     #js {:className "glyphicon glyphicon-remove form-control-feedback"})))

(defn- error-message [error]
  (if error             
    (dom/span #js {:className "help-block"} error)))
           
(defmethod form-group :default
  [[value error] owner {:keys [field]}]
  (reify
    om/IRenderState
    (render-state [_ state]
      (let [{:keys [key type title hidden?]} field]
        (if (= type :checkbox)
          (dom/div
           #js {:style (react-utils/hide hidden?)} 
           (om/build form-element value {:state state :opts {:field field}}))
          (dom/div
           #js {:className (str "form-group has-feedback" (if error " has-error"))
                :style (react-utils/hide hidden?)}
           (dom/label #js {:labelFor (name key) :className "control-label"} title)
           (om/build form-element value {:state state :opts {:field field}})
           (error-icon error)
           (error-message error)))))))


(defmethod form-group :inline
  [[value error] owner {:keys [field]}]
  (reify
    om/IRenderState
    (render-state [_ state]
      (let [{:keys [key type title hidden?]} field]
        (if (= type :checkbox)
          (dom/div
           #js {:className "form-group"
                :style (react-utils/hide hidden?)}
           (dom/div
            #js {:className "col-sm-10 col-sm-offset-2"}
            (om/build form-element value {:state state :opts {:field field}})))
          (dom/div
           #js {:className (str "form-group has-feedback" (if error " has-error"))
                 :style (react-utils/hide hidden?)}
           (dom/label #js {:labelFor (name key)
                           :className "control-label col-sm-2"} title)
           (dom/div
            #js {:className "col-sm-10"}
            (om/build form-element value {:state state :opts {:field field}})
            (error-icon error)
            (error-message error))))))))

(defn value-controlled
  [owner args field]
  (clj->js
   (merge {:className "form-control"
           :value (om/get-state owner :value)
           :disabled (:disabled? field)
           :onChange
           (fn [e]
             (let [v (react-utils/value e)]
               (om/set-state! owner :value v)
               (put! (om/get-state owner :ch) [field v])))}
          args (:args field))))

(defn checked-controlled
  [owner args field]
  (clj->js
   (merge {:checked (om/get-state owner :checked?)
           :disabled (:disabled? field)
           :onChange
           (fn [e]
             (let [c (react-utils/checked? e)]
               (om/set-state! owner :checked? c)
               (put! (om/get-state owner :ch) [field c])))}
          args (:args field))))

(defmethod form-element :default
  [value owner {:keys [field]}]
  (reify
    om/IInitState
    (init-state [_] {:value value})
    om/IRenderState
    (render-state [_ {:keys [value ch]}]
      (dom/input (value-controlled owner {} field)))))

(defmethod form-element :textarea
  [value owner {:keys [field]}]
  (reify
    om/IInitState
    (init-state [_] {:value value})
    om/IRenderState
    (render-state [_ {:keys [value ch]}]
      (dom/textarea
       (value-controlled owner {} field)))))

(defmethod form-element :checkbox
  [value owner {:keys [field]}]
  (reify
    om/IInitState
    (init-state [_] {:checked? value})
    om/IRenderState
    (render-state [_ {:keys [value ch]}]
      (dom/div
       #js {:className "checkbox"}
       (dom/label
        nil
        (dom/input
         (checked-controlled owner {:type "checkbox"
                                    :className "checkbox"} field)
         (dom/span #js {:className "label-text"} (:title field))))))))

(defmethod form-element :select
  [value owner {:keys [field]}]
  (reify
    om/IInitState
    (init-state [_] {:value value})
    om/IWillMount
    (will-mount [_]
      (let [options (:options field)]
        (if (fn? options)
          (let [ch (chan)]
            (go (om/set-state! owner :options (<! ch)))
            (options ch))
          (om/set-state! owner :options options))))
    om/IRenderState
    (render-state [_ {:keys [value ch options]}]
      (apply
       dom/select
       (value-controlled owner {} field)
       (for [{:keys [id text]} options]
         (dom/option #js {:value id} text))))))

(defmethod form-element :checkbox-group
  [value owner {:keys [field]}]
  (reify
    om/IInitState
    (init-state [_] {:value (set value)})
    om/IWillMount
    (will-mount [_]
      (let [options (:options field)]
        (if (fn? options)
          (let [ch (chan)]
            (go (om/set-state! owner :options (<! ch)))
            (options ch))
          (om/set-state! owner :options options))))
    om/IRenderState
    (render-state [_ {:keys [value ch options]}]
      (apply
       dom/div
       #js {:className "checkbox-group"}
       (for [{:keys [id text]} options]
         (dom/label
          #js {:className "checkbox"}
          (dom/input
           #js {:value id
                :checked (contains? value id)
                :className "checkbox"
                :type "checkbox"
                :disabled (:disabled? field)
                :onChange
                (fn [e]
                  (let [c (react-utils/checked? e)
                        v (if c
                            (set (conj value id))
                            (set (remove #(= id %) c)))]
                    (om/set-state! owner :value v)
                    (put! ch [field v])))})
          (dom/span #js {:className "label-text"} text)))))))

(defn form
  [app owner {:keys [group fields]}]
  (reify
    om/IWillMount
    (will-mount [_]
      (let [ch (chan)]
        (om/set-state! owner :ch ch)
        (go-loop [[field value] (<! ch)]
          (om/update! app (:key field) value)
          (recur (<! ch)))))
    om/IRenderState
    (render-state [_ {:keys [ch]}]
      (apply
       dom/div
       #js {:className "om-form"}
       (for [{:keys [key type] :as field} fields]
         (om/build form-group [(get app key) (get-in app [:errors key]) field]
                   {:opts {:group group
                           :field field}
                    :state {:ch ch}}))))))

