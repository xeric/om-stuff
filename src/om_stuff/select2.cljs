(ns om-stuff.select2
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan]]
            [om-stuff.react-utils :as react-utils]
            [om-stuff.core :as om-stuff :refer [form-element]]))

(defn init-select2
  [n field value data]
  (.select2 n (clj->js (merge {:data data} (:opts field))))
  (.trigger (.val n (clj->js value)) "change"))

(defn mount-select2
  [value owner field]
  (let [n (js/jQuery (om/get-node owner "select"))
        options (:options field)]
    (if (fn? options)
      (let [ch (chan)]
        (go (init-select2 n field value (<! ch)))
        (options ch))
      (init-select2 n field value options))
    (.on n "change"
         (fn [e]
           (let [v (.val n)]
             (om/set-state! owner :value v)
             (put! (om/get-state owner :ch) [field (js->clj v)]))))))

(defmethod form-element :select2
  [value owner {:keys [field]}]
  (reify
    om/IInitState
    (init-state [_] {:value value})
    om/IDidMount
    (did-mount [_] (mount-select2 value owner field))
    om/IRenderState
    (render-state [_ {:keys [value ch options]}]
      (dom/div
       #js {:className "select2-wrapper"}
       (apply
        dom/select
        #js {:className "form-control" :value value :ref "select"
             :disabled (:disabled? field)}
        (for [{:keys [id text]} options]
          (dom/option #js {:value id} text)))))))

(defmethod form-element :multi-select2
  [value owner {:keys [field]}]
  (reify
    om/IInitState
    (init-state [_] {:value (set value)})
    om/IDidMount
    (did-mount [_] (mount-select2 value owner field))
    om/IRenderState
    (render-state [_ {:keys [value ch options]}]
      (dom/div
       #js {:className "select2-wrapper"}
       (dom/select
        (clj->js
         (merge {:multiple "multiple"
                 :className "form-control"
                 :disabled (:disabled? field)
                 :ref "select"}
                (:args field))))))))
