(ns om-stuff.demo
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [om-stuff.select2 :as select2]
            [om-stuff.core :as om-stuff]
            [cljs.core.async :refer [put!]]))

(enable-console-print!)

(def fields [{:key :name :title "Name"}
             {:key :agree :title "Would you like to enter all details?" :type :checkbox}
             {:key :destination2 :title "Destination #2" :type :multi-select2 :disabled? true
              :options [{:id "Srinagar" :text "Srinagar"} {:id "Jammu" :text "Jammu"}]}

             {:key :destination :title "Destination" :type :multi-select2
              :options [{:id "Srinagar" :text "Srinagar"} {:id "Jammu" :text "Jammu"}]}
             {:key :random :title "Random" :type :select2
              :options (fn [ch]
                         (put! ch (repeatedly 5 (fn []
                                                  (let [s (rand-int 5)]
                                                    {:id s :text s})))))}
             {:key :participated :title "Participated" :type :checkbox-group
              :hidden? true :options [{:id "Yes" :text "Yes"} {:id "No" :text "No"}]}
             {:key :gender :title "Gender" :type :select :hidden? false
              :options [{:id "M" :text "Male"} {:id "F" :text "Female"}]}
             {:key :address :title "Address" :type :textarea :args {:rows 4}}])

(defn filter-fields
  [fields app]
  (mapv (fn [f]
          (if (contains? #{:participated :gender} (:key f))
            (assoc f :hidden? (not (:agree app)))
            f))
        fields))

(defn demo
  [app owner]
  (reify
    om/IRenderState
    (render-state [_ state]
      (dom/form
       #js {:className "form-horizontal"}
       (dom/pre nil (pr-str app))
       (om/build om-stuff/form app
                 {:opts {:group :inline
                         :fields (filter-fields fields app)}})))))

(def el (.. js/document (getElementById "demo")))

(def app-state (atom {:name "John" :agree false :gender "F"
                      :destination ["Srinagar"] :destination2 ["Jammu"]
                      :random "3" :participated #{"Yes"} :address "Home"
                      :errors {:name "Can't be john" :destination "Can't be srinagar!"}}))

(om/root demo app-state {:target el})
